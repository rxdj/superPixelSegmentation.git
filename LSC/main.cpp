#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/utility.hpp>

#include "lsc.hpp"

#include <ctype.h>
#include <stdio.h>
#include <iostream>

using namespace cv;
using namespace cv::ximgproc;
using namespace std;

int main(int argc, char** argv)
{
    int region_size = 15;
    int min_element_size = 50;
    int num_iterations = 3;

    Mat input_image = imread("data/lena.jpg", 1);

    Mat result, mask;

	for (;;)
	{
		Mat frame;
		input_image.copyTo(frame);

		if (frame.empty())
			break;

		result = frame;
		Mat converted;
		cvtColor(frame, converted, COLOR_BGR2HSV);

		Ptr<SuperpixelLSC> lsc = createSuperpixelLSC(converted, region_size);
		lsc->iterate(num_iterations);
		if (min_element_size > 0)
			lsc->enforceLabelConnectivity(min_element_size);

		cout << lsc->getNumberOfSuperpixels() << " superpixels" << endl;

		// get the contours for displaying
		lsc->getLabelContourMask(mask, true);
		result.setTo(Scalar(0, 255, 0), mask);
		imshow("labels", result);
		cv::imwrite("data/labels.jpg", result);
		cv::waitKey(0);
	}
    return 0;
}
